# nakala-report
*Michael Nauge, Laboratoire FoReLLIS, Université de Poitiers*


Utilitaire de reporting de données déposées sur Nakala.



## Générer un tableur

Micro application web permettant la génération d'un tableur XLSX contenant le DOI de toutes les DATAS et tous les FILES d'une COLLECTION Nakala.


[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.huma-num.fr%2Fmnauge%2Fnakala-report/HEAD?urlpath=voila%2Frender%2FnklReport2spreadsheet.ipynb)

NB : J'ai ajouté l'extraction de quelques champs de métadonnées, mais il en manque encore...

### Démonstration vidéo

[![link to video](https://videotheque.univ-poitiers.fr/datas/thumbs/videos/4f56zpj3m8fpb4hdxsd7.jpg)](https://videotheque.univ-poitiers.fr/video.php?id=4f56zpj3m8fpb4hdxsd7&link=4dmbdtqlc751q3r318dk1bkzqd6z4p)




## Générer des graphiques, des catalogues, des visuels de communication (WIP)
Après l'extraction sous forme de tableur, 
il possible de réaliser de nombreuses mesures quantitatives et générer de nombreux graphiques pour faciliter leurs interprétations.

Il est également possible de générer un *catalogue de ressources* au format pdf par exemple.

Ou bien généré de beaux objets de communications visuels en exploitant le end-point IIIF.

Un prototype de toutes ces choses est disponible ici : [nklReport_beta.ipynb](nklReport_beta.ipynb)

NB : C'est une version (WIP), elle fonctionne en local mais pas encore sur myBinder/voilà...



